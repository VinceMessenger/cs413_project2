/* 
   Vince Messenger
   CS413 Project 2 - Puzzles
   5/22/16
   Hangman
*/


(function(){
var gameport = document.getElementById("gameport");

var running = false;  // boolean flag for game running
var first_run = true;
var incorrect_count = 0;  // count incorrect guesses
var you_win = new PIXI.Sprite(new PIXI.Texture.fromImage("you_win.png"));
var you_lose = new PIXI.Sprite(new PIXI.Texture.fromImage("you_lose.png"));
var text = new PIXI.Text("Press enter to play again.\nPress ESC to return to the main menu.", {font: "18px Arial"});

var width = 600;
var length = 600;

var renderer = PIXI.autoDetectRenderer(width, length, {backgroundColor: 0x5b6ee1});
gameport.appendChild(renderer.view);

// vars for all the different game screens
var stage = new PIXI.Container();   // game screen
var start_screen = new PIXI.Container();   // start screen
var end_screen = new PIXI.Container();     // game over screen
var credit_screen = new PIXI.Container();  // credits screen
var tutorial_screen = new PIXI.Container();
var current_stage = start_screen;

// add background image
//var bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));
var bg = new PIXI.Sprite(new PIXI.Texture.fromImage("main_menu.png"));
var go_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("main_menu.png"));   // game_over background
var start_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("main_menu.png"));   // start_screen background
var tut_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("main_menu.png"));
var cred_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("main_menu.png"));

stage.addChild(bg);
end_screen.addChild(go_bg);
start_screen.addChild(start_bg);
tutorial_screen.addChild(tut_bg);
credit_screen.addChild(cred_bg);

// create menu sprite animation
PIXI.loader
  .add("assets.json")
  .load(ready);

var theme;
// add audio
PIXI.loader.add("theme.wav").load(sound_ready);

function sound_ready() {
	theme = PIXI.audioManager.getAudio("theme.wav");
	theme.loop = true;
	theme.play();
}

function ready() {
  
  var frames = [];

  for (var i=1; i<=8; i++) {
    frames.push(PIXI.Texture.fromFrame('full_hangman' + i + '.png'));
  }

  runner = new PIXI.extras.MovieClip(frames);
  runner.position.x = 414;
  runner.position.y = 269;
  runner.animationSpeed = 0.147;
  runner.play();
  start_bg.addChild(runner);
}

// create Already entered sprite
var already_entered = new PIXI.Text("Guesses: ");
already_entered.position.y = 500;
already_entered.position.x = 5;
bg.addChild(already_entered);

// add full hangman to menu

// create hangman container
var hangman_container = new PIXI.Container();

// create hangman head
var head = new PIXI.Sprite(new PIXI.Texture.fromImage("smiley.png"));
var head_pos = {x: 217, y: -55};
head.position.x = 500;
head.position.y = 50;
hangman_container.addChild(head);

// create hangman body
var body = new PIXI.Sprite(new PIXI.Texture.fromImage("body.png"));
var body_pos = {x: 217, y: 23};  // was
body.position.x = 500;
body.position.y = 126;
hangman_container.addChild(body);

// create hangman legs
var right_leg = new PIXI.Sprite(new PIXI.Texture.fromImage("right_leg.png"));
right_leg_pos = {x: 219, y: 87};
right_leg.position.x = 502;
right_leg.position.y = 190;
hangman_container.addChild(right_leg);

var left_leg = new PIXI.Sprite(new PIXI.Texture.fromImage("left_leg.png"));
left_leg_pos = {x: 219, y: 87};
left_leg.position.x = 502;
left_leg.position.y = 190;
hangman_container.addChild(left_leg);

// create hangman arms
var right_arm = new PIXI.Sprite(new PIXI.Texture.fromImage("right_arm.png"));
right_arm_pos = {x: 219, y: 37};
right_arm.position.x = 502;
right_arm.position.y = 140;
hangman_container.addChild(right_arm);

var left_arm = new PIXI.Sprite(new PIXI.Texture.fromImage("left_arm.png"));
left_arm_pos = {x: 219, y: 37};
left_arm.position.x = 502;
left_arm.position.y = 140;
hangman_container.addChild(left_arm);

// reposition hangman_container
hangman_container.position.y += 160;

// create credits screen
var cred = new PIXI.Container();
cred_bg.addChild(cred);

var cred_text = new PIXI.Text("Developed by Vince Messenger\n          Messenger Games\n\nSpecial thanks to Thomas Back for\n   assisting with audio production!\n\nPress ESC to return to the main menu.", {font: "18px Arial"});
cred_bg.addChild(cred_text);
cred_text.position.x = 150;
cred_text.position.y = 220;

// create tutorial screen
var tut = new PIXI.Container();
tut.position.x = 260;
tut.position.y = 220;
tut_bg.addChild(tut);

var tut_text = new PIXI.Text("Use your keyboard to guess letters.\nToo many wrong guesses and you will\ncomplete the hangman!\n\nPress ESC to return to the main menu.", {font: "18px Arial"});
tut_bg.addChild(tut_text);
tut_text.position.x = 150;
tut_text.position.y = 220;

// create start screen menu
var menu = new PIXI.Container();
menu.position.x = 260;
menu.position.y = 220;
start_bg.addChild(menu);

// add play to menu
var play = new PIXI.Text("Play", {font: "35px Arial"});
menu.addChild(play);
play.interactive = true;
play.mouseover = hover_start;
play.mouseout = hover_end;

// play click function
play.click = function() {
	init();
}

// add tutorial to menu
var tutorial = new PIXI.Text("Tutorial", {font: "35px Arial"});
menu.addChild(tutorial);
tutorial.interactive = true;
tutorial.mouseover = hover_start;
tutorial.mouseout = hover_end;
tutorial.position.y = 40;
tutorial.position.x = -22;

// tutorial click function
tutorial.click = function() {
	current_stage = tutorial_screen;
}

// add credits to menu
var credits = new PIXI.Text("Credits", {font: "35px Arial"});
menu.addChild(credits);
credits.interactive = true;
credits.mouseover = hover_start;
credits.mouseout = hover_end;
credits.position.y = 80;
credits.position.x = -17;

// credits click
credits.click = function() {
	current_stage = credit_screen;
}

// define hover functions
function hover_start() {
	this.scale.x = 1.25;
	this.scale.y = 1.25;
}

function hover_end() {
	this.scale.x = 1;
	this.scale.y = 1;
}

//read file
var dict;
var xhr = new XMLHttpRequest();
xhr.open("GET", "dictionary.txt", false);
xhr.send();
dict = xhr.responseText;

// split dictionary file and get random word
dict = dict.split("\n");

var randomWord;
while (!randomWord || randomWord.length > 12 || randomWord.length < 5)   // make sure word is between 5 and 12 characters inclusively
	randomWord = dict[Math.floor(Math.random()* dict.length)].trim();   // get random word from dictionary

var char_array = randomWord.split("");

// create container for holding word in view
var word = new PIXI.Container();
word.position.y = 400;

var letters = new Array();  // array of letter sprites
for (var i = 0; i < char_array.length; i++) {
	var container = new PIXI.Container();
	var underline = new PIXI.Sprite(new PIXI.Texture.fromImage("underletter.png"));
	underline.position.y = 30;
	underline.position.x = -10;
	container.addChild(underline);
	container.position.x = (i*50);
	letters[i] = new PIXI.Text(char_array[i]);
	letters[i].value = char_array[i];
	container.addChild(letters[i]);
	letters[i].scale.x = 2;
	letters[i].scale.y = 2;
	word.addChild(container);
	letters[i].visible = false;
}

word.position.x = (250 - (20*randomWord.length));
bg.addChild(word);

// define keydownListener
function keydownListener(key) {
	var guessed_letter = "";

	switch (key.keyCode) {
		case 81:
			guessed_letter = 'q';
			break;
		case 87:
			guessed_letter = 'w';
			break;
		case 69:
			guessed_letter = 'e';
			break;
		case 82:
			guessed_letter = 'r';
			break;
		case 84:
			guessed_letter = 't';
			break;
		case 89:
			guessed_letter = 'y';
			break;
		case 85:
			guessed_letter = 'u';
			break;
		case 73:
			guessed_letter = 'i';
			break;
		case 79:
			guessed_letter = 'o';
			break;
		case 80:
			guessed_letter = 'p';
			break;
		case 65:
			guessed_letter = 'a';
			break;
		case 83:
			guessed_letter = 's';
			break;
		case 68:
			guessed_letter = 'd';
			break;
		case 70:
			guessed_letter = 'f';
			break;
		case 71:
			guessed_letter = 'g';
			break;
		case 72:
			guessed_letter = 'h';
			break;
		case 74:
			guessed_letter = 'j';
			break;
		case 75:
			guessed_letter = 'k';
			break;
		case 76:
			guessed_letter = 'l';
			break;
		case 90:
			guessed_letter = 'z';
			break;
		case 88:
			guessed_letter = 'x';
			break;
		case 67:
			guessed_letter = 'c';
			break;
		case 86:
			guessed_letter = 'v';
			break;
		case 66:
			guessed_letter = 'b';
			break;
		case 78:
			guessed_letter = 'n';
			break;
		case 77:
			guessed_letter = 'm';
			break;
	}

	if (key.keyCode == 27 && !running) {
		current_stage = start_screen;
	}

	if (key.keyCode == 13 && !running) {
		current_stage = stage;
		init();
	}

	// reveal letter on gameboard
	if (running && guessed_letter != "") {
		reveal_letter(guessed_letter);
		guessed_letter = "";
	}
	else {
		//todo
	}
 }

// add event listeners
document.addEventListener('keydown', keydownListener);

var guessed_letters = [];   // array for holding previous guesses
function reveal_letter(guess) {

	if (guessed_letters.indexOf(guess) == -1) {   // check if letter already guessed
		guessed_letters.push(guess);
		already_entered.text = "Guesses:\n" + guessed_letters;

		var found_letter = false;   // found letter flag
		for (var i = 0; i < letters.length; i++) {
			if (letters[i].value == guess) {
				letters[i].visible = true; // reveal correctly guess letter
				found_letter = true;   // set found_letter flag
			}
		}

		if (!found_letter){
			incorrect_count++;  // increment incorrect guess counter
			switch (incorrect_count) {
				case 1:
					createjs.Tween.get(head.position).to(head_pos, 1000, createjs.Ease.bounceOut);
					break;
				case 2:
					createjs.Tween.get(body.position).to(body_pos, 1000, createjs.Ease.bounceOut);
					break;
				case 3:
					createjs.Tween.get(left_leg.position).to(left_leg_pos, 1000, createjs.Ease.bounceOut);
					break;
				case 4:
					createjs.Tween.get(right_leg.position).to(right_leg_pos, 1000, createjs.Ease.bounceOut);
					break;
				case 5:
					createjs.Tween.get(right_arm.position).to(right_arm_pos, 1000, createjs.Ease.bounceOut);
					break;
				case 6:
					createjs.Tween.get(left_arm.position).to(left_arm_pos, 1000, createjs.Ease.bounceOut);
					game_over();
					break;

			}
		}

		// check win
		var win = true;
		for (var i = 0; i < letters.length; i++) {
			if (letters[i].visible == false)
				win = false;
		}

		if (win && running)
			game_won();
	}
	else {
		console.log("You already guessed that letter!");
	}
}

function game_over() {
	running = false;

	for (var i = 0; i < letters.length; i++) {
		letters[i].visible = true;   // display correct answer
	}

	bg.addChild(you_lose);
	createjs.Tween.get(you_lose.position).to({x: 295, y: 200}, 1000, createjs.Ease.bounceOut);

	bg.addChild(text);
	createjs.Tween.get(text.position).to({x: 147, y: 325}, 1000, createjs.Ease.bounceOut);
}

function game_won() {
	running = false;
	//go_bg.addChild(word);
	//current_stage = end_screen;

	bg.addChild(you_win);
	createjs.Tween.get(you_win.position).to({x: 295, y: 200}, 1000, createjs.Ease.bounceOut);

	bg.addChild(text);
	createjs.Tween.get(text.position).to({x: 147, y: 325}, 1000, createjs.Ease.bounceOut);
}

function animate() {
	requestAnimationFrame(animate);

    // if (!running) {   // make characters spin on game over
    // 	sprite.rotation += .1;
    // 	lil_bro.rotation -= .1;
    // }

    if (current_stage == end_screen || current_stage == stage) {
    	current_stage.addChild(hangman_container);
    	current_stage.addChild(already_entered);
	}

	renderer.render(current_stage);
}

function init() {
	you_win.position.x = 650;
	you_win.position.y = 320;
	
	you_lose.position.x = 700;
	you_lose.position.y = 470;

	text.position.x = -340;
	text.position.y = 325;
	
	running = true;
    incorrect_count = 0;
    guessed_letters = [];
    already_entered.text = "Guesses:\n" + guessed_letters;

	// create hangman head
	head.position.x = 500;
	head.position.y = 50;

	// create hangman body
	body.position.x = 500;
	body.position.y = 126;

	// create hangman legs
	right_leg.position.x = 502;
	right_leg.position.y = 190;

	left_leg.position.x = 502;
	left_leg.position.y = 190;

	// create hangman arms
	right_arm.position.x = 502;
	right_arm.position.y = 140;

	left_arm.position.x = 502;
	left_arm.position.y = 140;

	// get new random word
	randomWord = "";
	while (!randomWord || randomWord.length > 12 || randomWord.length < 5)   // make sure word is between 5 and 12 characters inclusively
		randomWord = dict[Math.floor(Math.random()* dict.length)].trim();   // get random word from dictionary

    char_array = randomWord.split("");

    word.removeChildren();

    letters = [];  // array of letter sprites
	for (var i = 0; i < char_array.length; i++) {
		var container = new PIXI.Container();
		var underline = new PIXI.Sprite(new PIXI.Texture.fromImage("underletter.png"));
		underline.position.y = 30;
		underline.position.x = -10;
		container.addChild(underline);
		container.position.x = (i*50);
		letters[i] = new PIXI.Text(char_array[i]);
		letters[i].value = char_array[i];
		container.addChild(letters[i]);
		letters[i].scale.x = 2;
		letters[i].scale.y = 2;
		word.addChild(container);
		letters[i].visible = false;
	}
	word.position.x = (250 - (20*randomWord.length));
	bg.addChild(word);

	current_stage = stage;
}

// function update() {
// 	setInterval(game, 15);
// }

 animate();
// update();
})();
